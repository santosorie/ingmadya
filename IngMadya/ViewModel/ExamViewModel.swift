//
//  ExamViewModel.swift
//  IngMadya
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class ExamViewModel {
    var onViewNeedUpdated: (() -> Void)?
    private var questions: [Question] = [] {
        didSet {
            onViewNeedUpdated?()
        }
    }
    
    var totalQuestions: Int {
        return questions.count
    }
    
    func cellInfo(with index: Int) -> (id: Int, question: String, rightAnswer: String, answers: [String]) {
        guard index < questions.count else {
            return (0, "", "", [])
        }
        let question = questions[index]
        print(question.question)
        return (question.id, question.question, question.rightAnswer, question.answers)
    }
    
    func questionNeedReview(with id: Int) {
        questions = questions.filter { $0.id != id }
    }
    
    func fetchJson() {
        if let path = Bundle.main.url(forResource: "questions", withExtension: "json") {
            do {
                let jsonData = try Data(contentsOf: path)
                questions = try JSONDecoder().decode([Question].self, from: jsonData)
            } catch let error{
                print("error: \(error.localizedDescription)")
            }
        }
    }
}
