//
//  HintViewController.swift
//  IngMadya
//
//  Created by Harrie Santoso on 01/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class HintViewController: UIViewController {

    private let messageBoard: MessageBoardVB = MessageBoardVB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMessageBoard()
    }
    
    private func setupMessageBoard() {
        messageBoard.compose { info in
            info.size = view.frame.size
            info.hintText =
            """
            test aja
            waw
            test aja
            waw
            test aja
            waw
            test aja
            waw
            test aja
            waw
            test aja
            waw
            """
        }
        
        messageBoard.center = view.center
        view.addSubview(messageBoard)
    }
}
