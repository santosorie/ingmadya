//
//  ScoreViewController.swift
//  IngMadya
//
//  Created by Harrie Santoso on 31/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {
    
    let scoreBoard: ScoreBoardVB = ScoreBoardVB()
    private var score: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareScoreBoard()
    }
    
    func set(_ score: Int) {
        self.score = score
    }
    
    func prepareScoreBoard() {
        scoreBoard.compose { info in
            info.size = view.frame.size
            info.scoreText = "\(score)"
        }
        
        scoreBoard.center = view.center
        view.addSubview(scoreBoard)
    }
}
