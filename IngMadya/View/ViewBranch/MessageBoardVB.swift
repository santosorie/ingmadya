//
//  MessageBoardVB.swift
//  IngMadya
//
//  Created by Harrie Santoso on 06/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class MessageBoardVB: UIView, ViewBranch {
    typealias BranchInfo = Info
    private var info: Info = Info()
    private lazy var hintTextView: UITextView = {
        let textView: UITextView = UITextView()
        self.addSubview(textView)
        return textView
    }()
    
    func makeScene(with info: Info) {
        self.info = info
        self.frame.size = info.size
        backgroundColor = .green
        sceneHint()
    }
    
    private func sceneHint() {
        let maxHeight: CGFloat = 0.8 * info.height
        let textViewWidth: CGFloat = info.width - info.padding.left - info.padding.right
        hintTextView.text = info.hintText
        hintTextView.frame.size = CGSize(width: textViewWidth, height: 0)
        hintTextView.sizeToFit()
        if hintTextView.frame.size.height > maxHeight {
            hintTextView.frame.size = CGSize(width: textViewWidth, height: maxHeight)
        }
        hintTextView.center = center
    }
    
    class Info: BaseInfo {
        var hintText: String = ""
    }
}
