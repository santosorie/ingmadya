//
//  SimpleButtonVB.swift
//  IngMadya
//
//  Created by Harrie Santoso on 30/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class SimpleButtonVB: UIView, ViewBranch {
    typealias BranchInfo = Info
    
    var info: Info = Info()
    private lazy var button: UIButton = {
        let button: UIButton = UIButton()
        self.addSubview(button)
        return button
    }()
    
    func computeSize(with info: Info) -> CGSize {
        let height: CGFloat = info.padding.top + info.padding.bottom
        
        return CGSize(width: info.width, height: height)
    }
    
    func makeScene(with info: Info) {
        self.info = info
        frame.size = info.size
        
        sceneButton()
    }
    
    private func sceneButton() {
        let buttonWidth: CGFloat = info.width * 0.5
        let buttonHeight: CGFloat = info.height - (info.padding.top + info.padding.bottom)
        button.setTitle(info.title, for: .normal)
        button.frame.size = CGSize(width: buttonWidth, height: buttonHeight)
        button.center = center
        button.backgroundColor = .brown
        button.addTarget(self, action: #selector(onTapButton), for: .touchDown)
    }
    
    @objc private func onTapButton() {
        info.onTapButton?()
    }

    class Info: BaseInfo {
        var title: String = ""
        var onTapButton: (() -> Void)?
    }
}
