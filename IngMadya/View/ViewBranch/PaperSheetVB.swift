//
//  PapprSheetVB.swift
//  IngMadya
//
//  Created by Harrie Santoso on 27/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class PaperSheetVB: UIView, ViewBranch {
    typealias BranchInfo = Info
    
    private var info: Info = Info()
    var number: Int {
        return info.number
    }
    var questionId: Int {
        return info.questionId
    }
    private lazy var questionLabel: UITextView = {
        let label: UITextView = UITextView()
        self.addSubview(label)
        return label
    }()
    private lazy var answersContainer: UIStackView = {
        let container: UIStackView = UIStackView()
        self.addSubview(container)
        return container
    }()
    private lazy var imageBackground: UIImageView = {
        let imageView: UIImageView = UIImageView()
        self.addSubview(imageView)
        return imageView
    }()
    private lazy var warningImage: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.isHidden = true
        self.addSubview(imageView)
        return imageView
    }()
    private lazy var hintButton: UIButton = {
        let button: UIButton = UIButton()
        self.addSubview(button)
        return button
    }()
    private var answerLabels: [UIView] = []
    weak var delegate: PaperSheetDelegate?
    
    func computeSize(with info: Info) -> CGSize {
        let width = UIScreen.main.bounds.width - (info.padding.left + info.padding.right)
        var height: CGFloat = info.padding.bottom + info.padding.top
        
        height += CGFloat(info.answerTexts.count) * info.answerLabelHeight
        let questionLabel: UITextView = UITextView()
        questionLabel.frame.size = CGSize(width: width, height: 0)
        questionLabel.text = info.questionText
        questionLabel.font = UIFont.boldSystemFont(ofSize: 13)
        questionLabel.sizeToFit()
        let questionLabelSize: CGSize = questionLabel.frame.size
        height += questionLabelSize.height
        height += info.questionLabelBottomMargin
        
        return CGSize(width: width, height: height)
    }
    
    func makeScene(with info: Info) {
        self.info = info
        checkExamHasRightAnswer()
        frame.size = info.size
        backgroundColor = .gray
        
        sceneBackground()
        sceneQuestion()
        sceneAnswersContainer()
        sceneHint()
        sceneWarning()
    }
    
    private func sceneBackground() {
        imageBackground.frame = self.frame
        imageBackground.image = UIImage(named: "paper")
    }
    
    private func sceneQuestion() {
        let questionOrigin: CGPoint = CGPoint(x: info.padding.left, y: info.padding.top)
        
        questionLabel.backgroundColor = .clear
        questionLabel.text = info.questionText
        questionLabel.isUserInteractionEnabled = false
        questionLabel.frame = CGRect(origin: questionOrigin,
                                     size: CGSize(width: info.width, height: 0))
        questionLabel.font = UIFont.boldSystemFont(ofSize: 13)
        questionLabel.sizeToFit()
        
        
    }
    
    private func sceneAnswersContainer() {
        let containerHeight: CGFloat = CGFloat(info.answerTexts.count) * info.answerLabelHeight
        let containerY: CGFloat = questionLabel.frame.maxY + info.questionLabelBottomMargin
        let containerOrigin: CGPoint = CGPoint(x: info.padding.left, y: containerY)
        let containerSize: CGSize = CGSize(width: info.size.width, height: containerHeight)
        answersContainer.frame = CGRect(origin: containerOrigin, size: containerSize)
        answersContainer.axis = .vertical
        answersContainer.distribution = .fillEqually
        answersContainer.backgroundColor = .blue

        for answerText in info.answerTexts {
            answersContainer.addArrangedSubview(creaateAnswerLabel(with: answerText))
        }
    }
    
    private func sceneWarning() {
        let imageSize: CGSize = CGSize(width: 17, height: 17)
        let imageX: CGFloat = info.width - imageSize.width
        warningImage.frame = CGRect(origin: CGPoint(x: imageX, y: 0),
                                    size: imageSize)
        warningImage.image = UIImage(named: "redCorner")
        if info.showWarning {
            warningImage.isHidden = !info.tappedAnswer.isEmpty
        }
    }
    
    private func sceneHint() {
        let buttonSize: CGSize = CGSize(width: 29, height: 29)
        let buttonX: CGFloat = info.width - buttonSize.width - info.padding.right
        let buttonY: CGFloat = info.height - buttonSize.height - info.padding.bottom
        
        hintButton.frame = CGRect(origin: CGPoint(x: buttonX, y: buttonY),
                                  size: buttonSize)
        hintButton.setImage(UIImage(named: "hint"), for: .normal)
        hintButton.addTarget(self, action: #selector(didTapHintButton), for: .touchDown)
    }
    
    @objc private func didTapHintButton() {
        delegate?.didTapHintButton(on: self)
    }
    
    private func creaateAnswerLabel(with answer: String) -> LabelWithPickerVB {
        let labelWithPicker: LabelWithPickerVB = LabelWithPickerVB()
        
        labelWithPicker.compose { info in
            info.width = self.info.width
            info.answerText = answer
            info.padding = self.info.answerLabelPadding
            info.onTap = { [weak self] tappedAnswer, tappedLabel in
                guard let self = self else { return }
                for view in self.answersContainer.arrangedSubviews {
                    if let view = view as? LabelWithPickerVB {
                        view.isTapped = false
                    }
                }
                tappedLabel.isTapped = true
                self.delegate?.didTapAnswer(tappedAnswer, isRightAnswer: tappedAnswer == self.info.rightAnswer, on: self)
            }
        }
        
        if info.tappedAnswer == answer {
            labelWithPicker.isTapped = true
        }
        
        return labelWithPicker
    }
    
    //called this func after set view info, to check
    private func checkExamHasRightAnswer() {
        let hasRightAnswer: Bool = info.answerTexts.contains(info.rightAnswer)
        if !hasRightAnswer {
            delegate?.didNotFoundRightAnswer(on: self)
        }
    }
    
    class Info: BaseInfo {
        fileprivate let answerLabelPadding: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        fileprivate let questionLabelBottomMargin: CGFloat = 7
        fileprivate let answerLabelHeight: CGFloat = 33
        
        var questionText: String = ""
        var answerTexts: [String] = []
        var rightAnswer: String = ""
        var questionId: Int = 0
        var number: Int = 0
        var tappedAnswer: String = ""
        var showWarning: Bool = false
    }
}

protocol PaperSheetDelegate: class {
    func didTapAnswer(_ answer: String, isRightAnswer: Bool, on paperSheet: PaperSheetVB)
    func didTapHintButton(on paperSheet: PaperSheetVB)
    func didNotFoundRightAnswer(on paperSheet: PaperSheetVB)
}
