//
//  LabelWithPickerVB.swift
//  IngMadya
//
//  Created by Harrie Santoso on 27/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class LabelWithPickerVB: UIView, ViewBranch {
    func computeSize(with info: Info) -> CGSize {
        var height: CGFloat = info.padding.top + info .padding.bottom
        height += info.imageSize.height
        
        return CGSize(width: info.width, height: height)
    }
    var isTapped: Bool = false {
        didSet {
            if isTapped {
                image.tintColor = .green
            } else {
                image.tintColor = .darkGray
            }
        }
    }
    
    typealias BranchInfo = Info
    
    private lazy var label: UILabel = {
        let label: UILabel = UILabel()
        self.addSubview(label)
        return label
    }()
    private lazy var image: UIImageView = {
        let image: UIImageView = UIImageView()
        self.addSubview(image)
        return image
    }()
    var info: Info = Info()
    
    func makeScene(with info: Info) {
        self.info = info
        
        frame.size = info.size
        makeGesture()
        sceneImage()
        sceneLabel()
    }
    
    private func makeGesture() {
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapSelf))
        addGestureRecognizer(gesture)
    }
    
    @objc private func onTapSelf() {
        info.onTap?(info.answerText, self)
    }
    
    private func sceneImage() {
        let imageOrigin: CGPoint = CGPoint(x: info.padding.left, y: info.padding.top)
        let imageSize: CGSize = info.imageSize
        image.frame = CGRect(origin: imageOrigin,
                             size: imageSize)
        image.image = UIImage(named: "circle")?.withRenderingMode(.alwaysTemplate)
        image.tintColor = .darkGray
    }
    
    private func sceneLabel() {
        var labelX: CGFloat {
            return info.padding.left + image.frame.maxX + info.labelLeftMargin
        }
        var labelWidth: CGFloat {
            return info.width - (labelX + info.padding.right)
        }
        let labelOrigin: CGPoint = CGPoint(x: labelX, y: info.padding.top)
        let labelSize: CGSize = CGSize(width: labelWidth, height: 17)
        
        label.frame = CGRect(origin: labelOrigin,
                             size: labelSize)
        label.font = UIFont(name: label.font.fontName, size: 13)
        label.text = info.answerText
    }
    
    class Info: BaseInfo {
        fileprivate let imageSize: CGSize = CGSize(width: 23, height: 23)
        fileprivate let labelLeftMargin: CGFloat = 5

        var answerText: String = ""
        var onTap: ((String, LabelWithPickerVB) -> Void)?
    }
}
