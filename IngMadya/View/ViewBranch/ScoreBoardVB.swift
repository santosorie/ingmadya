//
//  ScoreBoardVB.swift
//  IngMadya
//
//  Created by Harrie Santoso on 01/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class ScoreBoardVB: UIView, ViewBranch {
    typealias BranchInfo = Info
    
    private var info: Info = Info()
    private lazy var scoreLabel: UILabel = {
        let label: UILabel = UILabel()
        self.addSubview(label)
        return label
    }()
    private lazy var cheerLabel: UILabel = {
        let label: UILabel = UILabel()
        self.addSubview(label)
        return label
    }()
    
    func makeScene(with info: Info) {
        self.info = info
        frame.size = info.size
        backgroundColor = .blue
        
        sceneScore()
        sceneCheer()
    }
    
    private func sceneScore() {
        scoreLabel.font = UIFont.boldSystemFont(ofSize: 41)
        scoreLabel.text = info.scoreText
        scoreLabel.sizeToFit()
        scoreLabel.center = center
    }
    
    private func sceneCheer() {
        cheerLabel.font = UIFont(name: cheerLabel.font.fontName, size: 23)
        cheerLabel.text = "Yay! you got"
        cheerLabel.sizeToFit()
        
        let cheerLabelBottomMargin: CGFloat = 13
        let cheerLabelY: CGFloat = scoreLabel.frame.minY - cheerLabel.frame.height - cheerLabelBottomMargin
        cheerLabel.frame.origin = CGPoint(x: center.x - (cheerLabel.frame.size.width / 2),
                                          y: cheerLabelY)
    }
    
    class Info: BaseInfo {
        var scoreText: String = ""
    }
}
