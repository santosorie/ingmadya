//
//  ExamTableAdapter.swift
//  IngMadya
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class ExamTableAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    var tableView: UITableView?
    var viewModel: ExamViewModel?
    
    init(tableView: UITableView, viewModel: ExamViewModel) {
        self.tableView = tableView
        self.viewModel = viewModel
    }
    
    enum Sections: Int {
        case exam
        case confirm
        case count
    }
    
    private var answers: [Int: (String, Bool)] = [:]
    private var unansweredRows: [Int] = []
    private var showWarningUnanswered: Bool = false
    
    //TODO: Move to constant
    private var cellIdentifier: String {
        return "cellIdentifier"
    }
    private var confirmButtonIdentifier: String {
        return "confirmButtonIdentifier"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        Sections.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.exam.rawValue {
            return viewModel?.totalQuestions ?? 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if indexPath.section == Sections.exam.rawValue {
            cell = examCell(tableView, for: indexPath)
            if !unansweredRows.contains(indexPath.row) && answers[indexPath.row] == nil {
                unansweredRows.append(indexPath.row)
            }
        } else {
            cell = confirmButtonCell(tableView, for: indexPath)
        }
        
        return cell
    }
    
    private func examCell(_ tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let cellInfo = viewModel.cellInfo(with: indexPath.row)
        let paperSheet: PaperSheetVB = PaperSheetVB()
        paperSheet.delegate = self
        paperSheet.compose { info in
            info.answerTexts = cellInfo.answers
            info.rightAnswer = cellInfo.rightAnswer
            info.questionText = cellInfo.question
            info.questionId = cellInfo.id
            info.padding = UIEdgeInsets(top: 13, left: 7, bottom: 13, right: 7)
            info.number = indexPath.row
            info.showWarning = showWarningUnanswered
            if let answer = answers[indexPath.row] {
                info.tappedAnswer = answer.0
            }
        }
        paperSheet.frame.origin = CGPoint(x: 7, y: 13)
        cell.contentView.addSubview(paperSheet)
        
        return cell
    }
    
    private func confirmButtonCell(_ tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: confirmButtonIdentifier, for: indexPath)
        let simpleButton: SimpleButtonVB = SimpleButtonVB()
        simpleButton.compose { info in
            info.width = UIScreen.main.bounds.width
            info.height = 51
            info.title = "selesai"
            info.padding = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0)
            info.onTapButton = { [weak self] in
                self?.checkQuestionsAnswered()
            }
        }
        cell.contentView.addSubview(simpleButton)
        
        return cell
    }
    
    //MARK: tableDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == Sections.exam.rawValue {
            return getPaperSize(on: indexPath.row)
        } else {
            let buttonHeight: CGFloat = 51
            let verticalPadding: CGFloat = 14
            return buttonHeight + verticalPadding
        }
    }
    
    private func getPaperSize(on number: Int) -> CGFloat {
        guard let viewModel = viewModel else { return 0 }
        let cellInfo = viewModel.cellInfo(with: number)
        let paperSheet: PaperSheetVB = PaperSheetVB()
        let paperInfo: PaperSheetVB.Info = PaperSheetVB.Info()
        paperInfo.answerTexts = cellInfo.answers
        paperInfo.padding = UIEdgeInsets(top: 13, left: 7, bottom: 13, right: 7)
        paperInfo.questionText = cellInfo.question
        
        let paperSize: CGSize = paperSheet.computeSize(with: paperInfo)
        
        return paperSize.height + 29
    }
    
    private func checkQuestionsAnswered() {
        guard unansweredRows.isEmpty else {
            showWarningUnanswered = true
            tableView?.scrollToRow(at: IndexPath(row: unansweredRows.first!, section: Sections.exam.rawValue),
                                  at: .top,
                                  animated: true)
            tableView?.reloadData()
            return
        }
        
        showWarningUnanswered = false
        tableView?.reloadData()
//        router.goToScore(with: navigationController, score: countScore())
    }
    
    private func countScore() -> Int {
           var score: Int = 0
           for (_, answer) in answers {
               let isRightAnswer: Bool = answer.1
               if isRightAnswer {
                   score += 1
               }
           }
           
           return score * 10
       }
}

extension ExamTableAdapter: PaperSheetDelegate {
    func didTapHintButton(on paperSheet: PaperSheetVB) {
//        router.goToHint(with: navigationController, hintText: "yuk marrie")
    }
    
    func didTapAnswer(_ answer: String, isRightAnswer: Bool, on paperSheet: PaperSheetVB) {
        unansweredRows.removeAll { row -> Bool in
            return row == paperSheet.number
        }
        answers[paperSheet.number] = (answer, isRightAnswer)
    }
    
    func didNotFoundRightAnswer(on paperSheet: PaperSheetVB) {
        print("tak ada jawaban benar")
        viewModel?.questionNeedReview(with: paperSheet.questionId)
    }
}
