//
//  ExamViewController.swift
//  IngMadya
//
//  Created by Harrie Santoso on 27/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class ExamViewController: UIViewController {
    
    private var tableView: UITableView = UITableView()
    
    lazy var tableViewAdapter = ExamTableAdapter(tableView: tableView,
                                                 viewModel: viewModel)
    
    private var cellIdentifier: String {
        return "cellIdentifier"
    }
    private var confirmButtonIdentifier: String {
        return "confirmButtonIdentifier"
    }
    
    lazy var viewModel: ExamViewModel = {
        let viewModel: ExamViewModel = ExamViewModel()
        viewModel.onViewNeedUpdated = {
            self.tableView.reloadData()
        }
        return viewModel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
        viewModel.fetchJson()
    }
    
    private func prepareTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: confirmButtonIdentifier)
        tableView.delegate = tableViewAdapter
        tableView.dataSource = tableViewAdapter
        tableView.frame = view.bounds
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 13, left: 0, bottom: 59, right: 0)
        view.addSubview(tableView)
    }
}
