//
//  ViewBranch.swift
//  IngMadya
//
//  Created by Harrie Santoso on 27/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class BaseInfo {
    required public init() {

    }
    var padding: UIEdgeInsets = .zero
    var size: CGSize {
        get {
            return CGSize(width: width, height: height)
        }
        set {
            width = newValue.width
            height = newValue.height
        }
    }
    var width: CGFloat = 0
    var height: CGFloat = 0
}

protocol ViewBranch {
    associatedtype BranchInfo: BaseInfo
    
    func compose(builder: (BranchInfo) -> Void)
    func computeSize(with info: BranchInfo) -> CGSize
    func makeScene(with info: BranchInfo)
}

extension ViewBranch {
    func compose(builder: (BranchInfo) -> Void) {
        let info: BranchInfo = BranchInfo()
        builder(info)
        
        if info.width == 0 || info.height == 0 {
            info.size = computeSize(with: info)
        }
        
        makeScene(with: info)
    }
    
    func computeSize(with info: BranchInfo) -> CGSize {
        return .zero
    }
}
