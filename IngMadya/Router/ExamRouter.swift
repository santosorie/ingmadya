//
//  ScoreRouter.swift
//  IngMadya
//
//  Created by Harrie Santoso on 01/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

protocol ExamRouterInterface {
    func goToScore(with navigation: UINavigationController?, score: Int)
    func goToHint(with navigation: UINavigationController?, hintText: String)
}

class ExamRouter: ExamRouterInterface {
    func goToScore(with navigation: UINavigationController?, score: Int) {
        let scoreViewController: ScoreViewController = ScoreViewController()
        scoreViewController.set(score)
        navigation?.pushViewController(scoreViewController, animated: true)
    }
    
    func goToHint(with navigation: UINavigationController?, hintText: String) {
        let hintViewController: HintViewController = HintViewController()
        navigation?.pushViewController(hintViewController, animated: true)
    }
}
