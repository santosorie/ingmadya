//
//  Question.swift
//  IngMadya
//
//  Created by Harrie Santoso on 30/03/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class Question: Decodable {
    var id: Int
    var question: String
    var answers: [String]
    var rightAnswer: String
}
